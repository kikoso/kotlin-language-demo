package com.enrique.demos.kotlin.beams;

/**
 * Created by enriquelopezmanas on 10/03/2017.
 */
public class CustomerJava {

	private int id;
	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
