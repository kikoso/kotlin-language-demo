package com.enrique.demos.kotlin.beams

/**
 * Created by enriquelopezmanas on 10/03/2017.
 */


data class CustomerKotlin(val id: Int = 0, var name: String = "")