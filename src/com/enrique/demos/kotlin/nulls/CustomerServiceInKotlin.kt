package com.enrique.demos.kotlin.nulls

import com.enrique.demos.kotlin.beams.CustomerJava

/**
 * Created by enriquelopezmanas on 10/03/2017.
 */

class CustomerServiceInKotlin {

    fun validateCustomer(customer: CustomerJava) {
        if (customer.name.startsWith("AA")) {
            throw SecurityException("Names are not allowed to start with AA")
        }
        if (customer.id == -1) {
            throw SecurityException("IDs cannot be -1")
        }
    }

    /*
    //This object can never be nullable
       fun validateCustomer(customer: CustomerKotlin) {
        if (customer.name.startsWith("AA")) {
            throw SecurityException("Names are not allowed to start with AA")
        }
        if (customer.id == -1) {
            throw SecurityException("IDs cannot be -1")
        }
    }
     */

    /*
    //This would be nullable
      fun validateCustomer(customer: CustomerKotlin?) {
       if (customer.name.startsWith("AA")) {
           throw SecurityException("Names are not allowed to start with AA")
       }
       if (customer.id == -1) {
           throw SecurityException("IDs cannot be -1")
       }
   }
    */
}