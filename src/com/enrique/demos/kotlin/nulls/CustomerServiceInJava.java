package com.enrique.demos.kotlin.nulls;

import com.enrique.demos.kotlin.beams.CustomerJava;

/**
 * Created by enriquelopezmanas on 10/03/2017.
 */
public class CustomerServiceInJava {

	public void validateCustomerInJava(CustomerJava customerJava) throws SecurityException {
		if (customerJava != null) {
			if (customerJava.getName() == null) {
				if (!customerJava.getName().contains("AA")) {
					throw new SecurityException("Names are not allowed to start with AA");
				}
			}

			if (customerJava.getId() == -1) {
					throw new SecurityException("ID cannot be -1");
			}
		}
	}
}
