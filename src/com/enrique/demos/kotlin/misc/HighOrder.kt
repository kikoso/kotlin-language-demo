package com.enrique.demos.kotlin.misc

import java.io.Closeable

/**
 * Created by enriquelopezmanas on 10/03/2017.
 */

fun highOrder(func: (Int, Int) -> Int) {
    println(func(2,3))
}

fun mySum(x: Int, y: Int): Int {
    return x + y
}

fun using(obj: Closeable, action: () -> Unit) {
    try {
        action()
    }   finally {
        obj.close()
    }
}

fun main(arg: Array<String>) {
    //highOrder(::mySum)
    highOrder { x, y -> x*y  }

   /*
    using(o) {
    }
     */



}