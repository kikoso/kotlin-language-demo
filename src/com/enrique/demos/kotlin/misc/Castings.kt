package com.enrique.demos.kotlin.misc

/**
 * Created by enriquelopezmanas on 10/03/2017.
 */

//By default Kotlin classes are final. Who is happy about that?
open class Person {

}

class Employee(val vacationDays: Int) : Person() {

}

class contractor: Person()

fun validateVacations(person: Person) {
    if (person is Employee)  {

        //What is missing here? Casting! Show that this is doing a smart casting, point out on person

        if (person.vacationDays < 20 ) {
            println("You need to take some time off!")
        }
    }

}