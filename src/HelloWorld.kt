import com.enrique.demos.kotlin.beams.CustomerJava
import com.enrique.demos.kotlin.beams.CustomerKotlin

//Default packege in Kotlin is default
object Global {
    //Kotlin is very good in type inference, you dont need to specify the type
    //val PI: Double = 3.14
    val PI = 3.14
    //In this case, the compiler things is an integer. Here we can specify some things like 10L
    val longValue = 10
}

//In Kotlin we do not need like in Java to create a class, we can just create a function in a file.
fun main(args: Array<String>) {

    val hellowWorld = "Hello World!"
    println(hellowWorld)

    val customer = CustomerJava()
    var id = customer.getId()

    val customerKotlin = CustomerKotlin(1,"Enrique")

    val secondCustomerKotlin = customerKotlin.copy(2)

    println(secondCustomerKotlin)

    println(Global.PI)

    helloWorldWithDefaultParameters(secondMessage = "Value",message = "Another")

    unlimited("1")
    unlimited("1","2")

    val returnValue = unlimited()

    val (name, email, idd) = returnThreeValues()

    val (id2, name2) = CustomerKotlin(1,"Enrique")


    // This, like in Haskell, declares a list of numbers from 1 to 100
    val listOfNumbers = 1..100

    for (number in listOfNumbers) {
        println(number)
    }

    val listOfPairs = listOf(Pair("Madrid","Spain"), "Paris" to "France")

    for ((city, country) in listOfPairs) {
        println(city)
    }
}

//By default, Unit is like Void, and is the default return type.
// Also implementation to String is a unit
fun helloWorld(message: String): Unit {
    println("HelloWorld")
}

fun addNumbers(x: Int, y: Int): Int {
    return x+y
}

fun addMoreNumbers(x: Int,y: Int) = x + y

fun addMoreNumbersWithReturn(x: Int,y: Int) : Int = x + y

//By default, Unit is like Void, and is the default return type.
// Also implementation to String is a unit
fun helloWorldWithDefaultParameters(message: String = "", secondMessage: String): Unit {
    println("HelloWorld")
}



fun unlimited(vararg arguments: String) {

}


fun returnTwoValues() : Pair<String, String> {
    return Pair("name", "email")
}

fun returnThreeValues() : Triple<String, String, String> {
    return Triple("name", "email", "id")
}

//if you want to return more values use a data class